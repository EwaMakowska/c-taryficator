#include <iostream>

class Fine {
public:
    void toPay(int speed, int maxSpeed) {
        if (speed - maxSpeed > 10 && speed - maxSpeed < 20) {
            std::cout << "Mandat 100 zł";
        }
        else if (speed - maxSpeed > 20 && speed - maxSpeed < 30) {
            std::cout << "Mandat 200 zł";
        }
        else if (speed - maxSpeed > 30 && speed - maxSpeed < 40) {
            std::cout << "Mandat 300 zł";
        }
        else if (speed - maxSpeed > 40 && speed - maxSpeed < 50) {
            std::cout << "Mandat 400 zł";
        }
        else if (speed - maxSpeed > 50) {
            std::cout << "Mandat 500 zł";
        }
        else if (speed - maxSpeed < 10) {
            std::cout << "Zgodnie z prawem";
        }
    }
};

int main()
{
    int speed;
    int zone;
    std::cout << "Podaj prędkość:";
    std::cin >> speed;
    std::cout << "Wybierz strefę: \n 1 - zabudowana \n 2 - niezabudowana \n 3 - autostrada";
    std::cin >> zone;

    Fine fine;

    switch (zone) {
        case 1:
            fine.toPay(speed, 50);
            break;
        case 2:
            fine.toPay(speed, 90);
            break;
        case 3:
            fine.toPay(speed, 130);
            break;
        default: 
            fine.toPay(speed, 50);
            break;
    }

}
